package ru.kuzstu.abiturientkuzstu;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdapterSpecialty {
    final String ATTR_GROUP_NAME= "lvlName";
    final String ATTR_SPECIALTY_KOD= "specialtyKod";
    final String ATTR_SPECIALTY_NAME = "specialtyName";

    // названия форм обучения (групп)
    String[] groups;

    ArrayList<SpecialtyClass> bakalavr;
    ArrayList<SpecialtyClass> specialist;
    ArrayList<SpecialtyClass> magistr;

    // коллекция для групп
    ArrayList<Map<String, String>> groupData;
    // коллекция для элементов одной группы
    ArrayList<Map<String, String>> childDataItem;
    // общая коллекция для коллекций элементов
    ArrayList<ArrayList<Map<String, String>>> childData;
    // в итоге получится childData = ArrayList<childDataItem>
    // список аттрибутов группы или элемента
    Map<String, String> m;

    Context ctx;

    public dbHandler dbH;

    AdapterSpecialty(Context _ctx) {
        ctx = _ctx;
        dbH = new dbHandler(ctx,true);

        groups = ctx.getResources().getStringArray(R.array.title_group_specialty_item);

        bakalavr = dbH.getBakalavr();
        specialist = dbH.getSpecialist();
        magistr = dbH.getMagistr();
    }

    SimpleExpandableListAdapter adapter;

    SimpleExpandableListAdapter getAdapter() {
        // заполняем коллекцию групп из массива с названиями групп
        groupData = new ArrayList<Map<String, String>>();
        for (String group : groups) {
            // заполняем список аттрибутов для каждой группы
            m = new HashMap<String, String>();
            m.put(ATTR_GROUP_NAME, group); // имя компании
            groupData.add(m);
        }

        // список аттрибутов групп для чтения
        String groupFrom[] = new String[] {ATTR_GROUP_NAME};
        // список ID view-элементов, в которые будет помещены аттрибуты групп
        int groupTo[] = new int[] {android.R.id.text1};

        // создаем коллекцию для коллекций элементов
        childData = new ArrayList<ArrayList<Map<String, String>>>();

        // создаем коллекцию элементов для первой группы
        childDataItem = new ArrayList<Map<String, String>>();
        // заполняем список аттрибутов для каждого элемента
        for (SpecialtyClass tm : bakalavr) {
            m = new HashMap<String, String>();
            m.put(ATTR_SPECIALTY_NAME, tm.specialty_name);
            m.put(ATTR_SPECIALTY_KOD, tm.kod);
            childDataItem.add(m);
        }
        // добавляем в коллекцию коллекций
        childData.add(childDataItem);

        // создаем коллекцию элементов для второй группы
        childDataItem = new ArrayList<Map<String, String>>();
        for (SpecialtyClass tm : specialist) {
            m = new HashMap<String, String>();
            m.put(ATTR_SPECIALTY_NAME, tm.specialty_name);
            m.put(ATTR_SPECIALTY_KOD, tm.kod);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        // создаем коллекцию элементов для третьей группы
        childDataItem = new ArrayList<Map<String, String>>();
        for (SpecialtyClass tm : magistr) {
            m = new HashMap<String, String>();
            m.put(ATTR_SPECIALTY_NAME, tm.specialty_name);
            m.put(ATTR_SPECIALTY_KOD, tm.kod);
            childDataItem.add(m);
        }
        childData.add(childDataItem);

        // список аттрибутов элементов для чтения
        String childFrom[] = new String[] {ATTR_SPECIALTY_NAME,ATTR_SPECIALTY_KOD};
        // список ID view-элементов, в которые будет помещены аттрибуты элементов
        //int childTo[] = new int[] {android.R.id.text1};
        int childTo[] = new int[] {R.id.tvNameSpecialty, R.id.tvKodSpecialty};

        adapter = new SimpleExpandableListAdapter(
                ctx,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                //R.layout.item_view_groop_spetialty,
                groupFrom,
                groupTo,
                childData,
                R.layout.item_spetialty,
                childFrom,
                childTo);

        return adapter;
    }

    String getGroupText(int groupPos) {
        return ((Map<String,String>)(adapter.getGroup(groupPos))).get(ATTR_GROUP_NAME);
    }

    String getChildKod(int groupPos, int childPos) {
        return ((Map<String,String>)(adapter.getChild(groupPos, childPos))).get(ATTR_SPECIALTY_KOD);
    }

    String getGroupChildText(int groupPos, int childPos) {
        return getChildKod(groupPos, childPos);
    }
}
