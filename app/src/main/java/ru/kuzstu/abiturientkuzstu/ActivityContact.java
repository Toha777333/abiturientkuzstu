package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class ActivityContact extends ActionBarActivity implements OnClickListener {
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_contact);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();

        //прикручивание нажатие на телефон
        LinearLayout btContactOtherPhone = (LinearLayout)findViewById(R.id.btContactOtherPhone);
        btContactOtherPhone.setOnClickListener(this);

        LinearLayout btContactOtherSite = (LinearLayout)findViewById(R.id.btContactOtherSite);
        btContactOtherSite.setOnClickListener(this);

        LinearLayout btContactOtherVK = (LinearLayout)findViewById(R.id.btContactOtherVK);
        btContactOtherVK.setOnClickListener(this);

        LinearLayout btContactPriemComissPhone = (LinearLayout)findViewById(R.id.btContactPriemComissPhone);
        btContactPriemComissPhone.setOnClickListener(this);

        LinearLayout btContactPriemComissFacs = (LinearLayout)findViewById(R.id.btContactPriemComissFacs);
        btContactPriemComissFacs.setOnClickListener(this);

        LinearLayout btContactPriemComissEmale = (LinearLayout)findViewById(R.id.btContactPriemComissEmale);
        btContactPriemComissEmale.setOnClickListener(this);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Uri callUri;
        Intent callIntent,browseIntent;

        switch (v.getId()) {
            case R.id.btContactOtherPhone:
                callUri = Uri.parse("tel:+7 (3842) 39-69-60");
                callIntent= new Intent(Intent.ACTION_DIAL, callUri);
                startActivity(callIntent);
                break;
            case R.id.btContactOtherSite:
                callUri = Uri.parse("http://www.kuzstu.ru");
                browseIntent = new Intent(Intent.ACTION_VIEW, callUri);
                startActivity(browseIntent);
                break;
            case R.id.btContactOtherVK:
                callUri = Uri.parse("http://vk.com/club47097257");
                browseIntent = new Intent(Intent.ACTION_VIEW, callUri);
                startActivity(browseIntent);
                break;
            case R.id.btContactPriemComissPhone:
                callUri = Uri.parse("tel:+7 (3842) 58-67-91");
                callIntent= new Intent(Intent.ACTION_DIAL, callUri);
                startActivity(callIntent);
                break;
            case R.id.btContactPriemComissFacs:
                callUri = Uri.parse("tel:+7 (3842) 58-33-80");
                callIntent= new Intent(Intent.ACTION_DIAL, callUri);
                startActivity(callIntent);
                break;
            case R.id.btContactPriemComissEmale:
                callUri = Uri.parse("mailto:priem@kuzstu.ru");
                browseIntent = new Intent(Intent.ACTION_VIEW, callUri);
                startActivity(browseIntent);
                break;
        }
    }
}
