package ru.kuzstu.abiturientkuzstu;

import java.io.Serializable;

public class SpecialtyClass implements Serializable {
    public String  kod;
    public String  specialty_name;
    public String  tip_obucheniya;
    public String  tip_ochnoe;
    public String  tip_zaochnoe;
    public String  tip_ochno_zaochnoe;
    public String  budget_mest_ochnyx;
    public String  budget_mest_zaochnyx;
    public String  contract_mest_ochnyx;
    public String  contract_mest_zaochnyx;
    public String  budget_mest_ochno_zaochnyx;
    public String  contract_mest_ochno_zaochnyx;
    public String  stoimost_ochnogo;
    public String  stoimost_zaochnogo;
    public String  stoimost_ochno_zaochnogo;
    public String  predmety_ege;
    public String  proxodnoj_ball_pozaproshlyj_god;
    public String  proxodnoj_ball_proshlyj_god;
    public String  opisanie;

    public SpecialtyClass(){}
    public SpecialtyClass(
            String _kod,
            String _specialty_name,
            String _tip_obucheniya,
            String _tip_ochnoe,
            String _tip_zaochnoe,
            String _tip_ochno_zaochnoe,
            String _budget_mest_ochnyx,
            String _budget_mest_zaochnyx,
            String _contract_mest_ochnyx,
            String _contract_mest_zaochnyx,
            String _budget_mest_ochno_zaochnyx,
            String _contract_mest_ochno_zaochnyx,
            String _stoimost_ochnogo,
            String _stoimost_zaochnogo,
            String _stoimost_ochno_zaochnogo,
            String _predmety_ege,
            String _proxodnoj_ball_pozaproshlyj_god,
            String _proxodnoj_ball_proshlyj_god,
            String _opisanie){
        kod = _kod;
        specialty_name = _specialty_name;
        tip_obucheniya = _tip_obucheniya;
        tip_ochnoe = _tip_ochnoe;
        tip_zaochnoe = _tip_zaochnoe;
        tip_ochno_zaochnoe = _tip_ochno_zaochnoe;
        budget_mest_ochnyx = _budget_mest_ochnyx;
        budget_mest_zaochnyx = _budget_mest_zaochnyx;
        contract_mest_ochnyx = _contract_mest_ochnyx;
        contract_mest_zaochnyx = _contract_mest_zaochnyx;
        budget_mest_ochno_zaochnyx = _budget_mest_ochno_zaochnyx;
        contract_mest_ochno_zaochnyx = _contract_mest_ochno_zaochnyx;
        stoimost_ochnogo = _stoimost_ochnogo;
        stoimost_zaochnogo = _stoimost_zaochnogo;
        stoimost_ochno_zaochnogo = _stoimost_ochno_zaochnogo;
        predmety_ege = _predmety_ege;
        proxodnoj_ball_pozaproshlyj_god = _proxodnoj_ball_pozaproshlyj_god;
        proxodnoj_ball_proshlyj_god = _proxodnoj_ball_proshlyj_god;
        opisanie = _opisanie;
    }
}
