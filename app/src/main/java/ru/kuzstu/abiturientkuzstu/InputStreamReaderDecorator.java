package ru.kuzstu.abiturientkuzstu;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class InputStreamReaderDecorator extends InputStreamReader
{
    private InputStream inputStream;

    public InputStreamReaderDecorator(InputStream inputStream)
    {
        super(inputStream);
        this.inputStream = inputStream;
    }

    public String readToEnd() throws IOException
    {
        byte[] buff = new byte[8192];

        int mLengthBuff,mFinalLengthBuff=0;

        byte[] bb = new byte[0];

        while ((mLengthBuff = inputStream.read(buff)) > 0)
        {
            mFinalLengthBuff+=mLengthBuff;

            bb = concatenate(bb,buff,mLengthBuff);
        }

        return new String(bb, 0, mFinalLengthBuff);
    }


    public byte[] concatenate (byte[] A, byte[] B, int _b) {
        int aLen = A.length;
        int bLen = _b;

        @SuppressWarnings("unchecked")
        byte[] C = (byte[]) Array.newInstance(A.getClass().getComponentType(), aLen + bLen);
        System.arraycopy(A, 0, C, 0, aLen);
        System.arraycopy(B, 0, C, aLen, bLen);

        return C;
    }
}