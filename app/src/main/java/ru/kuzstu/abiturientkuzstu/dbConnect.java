package ru.kuzstu.abiturientkuzstu;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;


public class dbConnect extends SQLiteOpenHelper {
    private Context context;

    final String LOG_TAG = "myLogs";

    /**
     * Класс для подключения к БД, при первом запуске программы создает новую БД
     * @param context объект, который предоставляет доступ к базовым функциям приложения
     */
    public dbConnect(Context context) {
        // конструктор суперкласса
        super(context, "myDB", null, 1);
        this.context = context;
        Log.e(LOG_TAG, "dbConnect: Создали подключение к бд myDB");
    }

    /**
     * метод создает таблицу если её нет
     * @param db подключение к БД
     */
    @Override
    public void onCreate(final SQLiteDatabase db) {
        //основной сервер
        final String urlVersion_json = "http://abitur-hosting.ucoz.com/version_json.txt";
        final String urlAbitur = "http://abitur-hosting.ucoz.com/abityr.txt";

        //создаем БД
        db.execSQL("create table version_json ("
                + "id integer primary key autoincrement,"
                + "version text);");

        Log.e(LOG_TAG, "dbConnect: создали базу данных версий");

        ContentValues cv = new ContentValues();
        cv.put("version",-1);
        db.insert("version_json", null, cv);

        Log.e(LOG_TAG, "dbConnect: добавили запись в версией данных -1");
        //БД с основными данными
        db.execSQL("create table abitur ("
                + "id integer primary key autoincrement,"
                + "kod text,"
                + "specialty_name text,"
                + "tip_obucheniya text,"
                + "tip_ochnoe text,"
                + "tip_zaochnoe text,"
                + "tip_ochno_zaochnoe text,"
                + "budget_mest_ochnyx text,"
                + "budget_mest_zaochnyx text,"
                + "contract_mest_ochnyx text,"
                + "contract_mest_zaochnyx text,"
                + "budget_mest_ochno_zaochnyx text,"
                + "contract_mest_ochno_zaochnyx text,"
                + "stoimost_ochnogo text,"
                + "stoimost_zaochnogo text,"
                + "stoimost_ochno_zaochnogo text,"
                + "predmety_ege text,"
                + "proxodnoj_ball_pozaproshlyj_god text,"
                + "proxodnoj_ball_proshlyj_god text,"
                + "opisanie text);");

        Log.e(LOG_TAG, "dbConnect: создали базу с основными данными");


       /* //поток для скачивания
        new Thread(new Runnable() {
            @Override
            public void run() {

                String text="";
                Log.e(LOG_TAG, "dbConnect: Начинаем скачивать файл с версией");
                try {
                    URL url = new URL(urlVersion_json);
                    InputStreamReaderDecorator inputStreamReaderDecorator =
                            new InputStreamReaderDecorator(url.openStream());
                    text = inputStreamReaderDecorator.readToEnd();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "dbConnect: ошибка при скачивание файла с версией. ", e);
                }
                Log.e(LOG_TAG, "dbConnect: закончили скачивать файл с версией");
                //сзаписываем результат в базу
                ContentValues cv = new ContentValues();
                cv.put("version",text);
                db.insert("version_json", null, cv);

                String textAbitur="";

                Log.e(LOG_TAG, "dbConnect: Начинаем скачивать файл с основными данными");
                try {
                    URL url = new URL(urlAbitur);
                    InputStreamReaderDecorator inputStreamReaderDecorator =
                            new InputStreamReaderDecorator(url.openStream());
                    textAbitur = inputStreamReaderDecorator.readToEnd();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "dbConnect: ошибка при скачивание файла с основными данными. ", e);
                    return;
                }

                //сохраняем файл в пямять устройства
                try{
                    // отрываем поток для записи
                    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                            context.openFileOutput("abitur.txt", 0)));
                    // пишем данные
                    bw.write(textAbitur);
                    Log.e(LOG_TAG, "dbConnect: файл записан ");
                    // закрываем поток
                    bw.close();
                    Log.e(LOG_TAG, "dbConnect: данные скачены, Файл записан. Закрыли поток записи ");
                } catch (IOException out) {
                    Log.e(LOG_TAG, "dbConnect: при скачивание файла с основными данными ", out);
                }

            }
        }).start();
*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}