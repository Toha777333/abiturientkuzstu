package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class ViewActivity extends ActionBarActivity {
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    String mKod;
    SpecialtyClass mSpecialty;
    dbHandler dbH;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);

        genericDrawerMenu();

        mKod = getIntent().getExtras().getString("kod", "");

        dbH = new dbHandler(this, true);
        mSpecialty = dbH.getSpecialty(mKod);

        if(mSpecialty.tip_obucheniya.equals("Бакалавр")){
            this.setTitle("Направление - " + mKod);
        }else{
            this.setTitle("Специальность - " + mKod);
        }

        TextView header = (TextView)findViewById(R.id.textSpecialtyName);
        header.setText(mSpecialty.specialty_name);

        if(!mSpecialty.tip_obucheniya.equals("Магистр")){
            LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewlayout);
            LayoutInflater ltInflater = getLayoutInflater();

            View item = ltInflater.inflate(R.layout.item_view_2, linLayout, false);

            TextView tvName = (TextView) item.findViewById(R.id.textView2Name);
            tvName.setText("Предмет по выбору:");

            TextView tvValue= (TextView) item.findViewById(R.id.textView2Value);
            tvValue.setText(mSpecialty.predmety_ege);

            linLayout.addView(item);
////////////////////////////////////////
            item = ltInflater.inflate(R.layout.item_view_2, linLayout, false);
//
            tvName = (TextView) item.findViewById(R.id.textView2Name);
            tvName.setText("Баллы за прошлый год:");

            tvValue= (TextView) item.findViewById(R.id.textView2Value);
            tvValue.setText(mSpecialty.proxodnoj_ball_proshlyj_god);

            linLayout.addView(item);
////////////////////////////////////////
            item = ltInflater.inflate(R.layout.item_view_2, linLayout, false);
            tvName = (TextView) item.findViewById(R.id.textView2Name);
            tvName.setText("Баллы за позапрошлый год:");

            tvValue= (TextView) item.findViewById(R.id.textView2Value);
            tvValue.setText(mSpecialty.proxodnoj_ball_pozaproshlyj_god);

            linLayout.addView(item);
        }

        if(mSpecialty.tip_ochnoe.equals("есть")){
            LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewlayout);
            LayoutInflater ltInflater = getLayoutInflater();

            View item = ltInflater.inflate(R.layout.item_view, linLayout, false);

            TextView tvTip = (TextView) item.findViewById(R.id.textTipObuchenie);
            tvTip.setText("Очное обучение");

            TextView tvBudgetKol= (TextView) item.findViewById(R.id.textBjudzhetKol);
            tvBudgetKol.setText(mSpecialty.budget_mest_ochnyx);

            TextView tvKontractKol = (TextView) item.findViewById(R.id.textKontractKol);
            tvKontractKol.setText(String.valueOf(mSpecialty.contract_mest_ochnyx));

            TextView tvKontractPrice = (TextView) item.findViewById(R.id.textKontractPrice);
            tvKontractPrice.setText(String.valueOf(mSpecialty.stoimost_ochnogo));
            //item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;

            linLayout.addView(item);
        }

        if(mSpecialty.tip_zaochnoe.equals("есть")){
            LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewlayout);
            LayoutInflater ltInflater = getLayoutInflater();

            View item = ltInflater.inflate(R.layout.item_view, linLayout, false);

            TextView tvTip = (TextView) item.findViewById(R.id.textTipObuchenie);
            tvTip.setText("Заочное обучение");

            TextView tvBudgetKol= (TextView) item.findViewById(R.id.textBjudzhetKol);
            tvBudgetKol.setText(mSpecialty.budget_mest_zaochnyx);

            TextView tvKontractKol = (TextView) item.findViewById(R.id.textKontractKol);
            tvKontractKol.setText(String.valueOf(mSpecialty.contract_mest_zaochnyx));

            TextView tvKontractPrice = (TextView) item.findViewById(R.id.textKontractPrice);
            tvKontractPrice.setText(String.valueOf(mSpecialty.stoimost_zaochnogo));
            //item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;

            linLayout.addView(item);
        }

        if(mSpecialty.tip_ochno_zaochnoe.equals("есть")){
            LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewlayout);
            LayoutInflater ltInflater = getLayoutInflater();

            View item = ltInflater.inflate(R.layout.item_view, linLayout, false);

            TextView tvTip = (TextView) item.findViewById(R.id.textTipObuchenie);
            tvTip.setText("Очно-заочное");

            TextView tvBudgetKol= (TextView) item.findViewById(R.id.textBjudzhetKol);
            tvBudgetKol.setText(mSpecialty.budget_mest_ochno_zaochnyx);

            TextView tvKontractKol = (TextView) item.findViewById(R.id.textKontractKol);
            tvKontractKol.setText(String.valueOf(mSpecialty.contract_mest_ochno_zaochnyx));

            TextView tvKontractPrice = (TextView) item.findViewById(R.id.textKontractPrice);
            tvKontractPrice.setText(String.valueOf(mSpecialty.stoimost_ochno_zaochnogo));
            //item.getLayoutParams().width = ViewGroup.LayoutParams.MATCH_PARENT;

            linLayout.addView(item);
        }

        if(!mSpecialty.opisanie.equals("-")){
            LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewlayout);
            LayoutInflater ltInflater = getLayoutInflater();

            View item = ltInflater.inflate(R.layout.item_view_3, linLayout, false);

            TextView tvName = (TextView) item.findViewById(R.id.textView3Name);
            tvName.setText("Чему учат?");

            TextView tvValue= (TextView) item.findViewById(R.id.textView3Value);
            tvValue.setText(mSpecialty.opisanie);

            linLayout.addView(item);
        }


    }

    public void genericDrawerMenu(){
        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_contact);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
