package ru.kuzstu.abiturientkuzstu;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.InputType;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.widget.Toolbar;

import java.util.ArrayList;

public class ActivityPodbor extends ActionBarActivity implements OnClickListener{
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar, actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    String[] data;
    int wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
    LinearLayout LayoutPredmetAdd;
    Button ButtonAdd, ButtonPodbor;
    OnItemSelectedListener ItemSelectedListener;
    AdapterView.OnItemClickListener ItemClickListener;

    dbHandler dbH;

    int mCountPredmetPoViboru;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_podbor);

        dbH = new dbHandler(this,true);
        mCountPredmetPoViboru=1;
        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        ButtonAdd = (Button)findViewById(R.id.buttonAdd);
        ButtonAdd.setOnClickListener(this);

        ButtonPodbor = (Button)findViewById(R.id.buttonPodborPodobrat);
        ButtonPodbor.setOnClickListener(this);

        //Вывадающий список
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        data = dbH.getMassPredmetEge();//getResources().getStringArray(R.array.array_predmety_ege);
        // адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_podbor, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        // заголовок
        spinner.setPrompt("Title");
        // выделяем элемент
        spinner.setSelection(0);

        // устанавливаем обработчик нажатия
        ItemSelectedListener = new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {


                // показываем позиция нажатого элемента
              //  Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                int i = 0;
            }
        };
        //Обработчик привязывается к списку
        spinner.setOnItemSelectedListener(ItemSelectedListener);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_podbor);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_activity_podbor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        LayoutPredmetAdd = (LinearLayout) findViewById(R.id.LayoutPredmetAdd);

        switch (v.getId()) {
            case R.id.buttonAdd:// кнопка Добавить

                if(mCountPredmetPoViboru>=4){
                    break;
                }
                LinearLayout linLayout = (LinearLayout) findViewById(R.id.LayoutPredmetAdd);
                LayoutInflater ltInflater = getLayoutInflater();

                View item = ltInflater.inflate(R.layout.item_podbor_predmet, linLayout, false);

                //Вывадающий список
                Spinner spinner = (Spinner) item.findViewById(R.id.item_podbor_predmet_spinner);
                data = dbH.getMassPredmetEge();//getResources().getStringArray(R.array.array_predmety_ege);
                // адаптер
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.item_podbor, data);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
                // заголовок
                spinner.setPrompt("Title");
                // выделяем элемент
                spinner.setSelection(0);

                Button buttonDel = (Button) item.findViewById(R.id.item_podbor_predmet_buttonDel);
                buttonDel.setOnClickListener(this);

                linLayout.addView(item);
                mCountPredmetPoViboru++;

                break;
            case R.id.buttonPodborPodobrat:
                LinearLayout LayoutParent = (LinearLayout) findViewById(R.id.layout_podbor_parrent);
                ArrayList<String> mResultPredmet = new ArrayList<String>();

                EditText etRussa = (EditText)LayoutParent.findViewById(R.id.editTextRussaPodbor);
                EditText etMatan = (EditText)LayoutParent.findViewById(R.id.editTextMatanPodbor);

                String tmBallRussa = etRussa.getText().toString();
                if(tmBallRussa.equals("")){
                    tmBallRussa = "0";
                }

                String tmBallMatan = etMatan.getText().toString();
                if(tmBallMatan.equals("")){
                    tmBallMatan = "0";
                }

                int sumBall = Integer.valueOf(tmBallRussa) + Integer.valueOf(tmBallMatan);

                EditText etext1 = (EditText)LayoutParent.findViewById(R.id.editTextPodbor1Podbor);

                String tmBallEtext1 = etext1.getText().toString();
                if(tmBallEtext1.equals("")){
                    tmBallEtext1 = "0";
                }
                int temp1 = sumBall + Integer.valueOf(tmBallEtext1);

                Spinner spinn1 = (Spinner)LayoutParent.findViewById(R.id.spinner);
                String name1 = spinn1.getSelectedItem().toString();

                String ball1 = String.valueOf(temp1);

                mResultPredmet.add(name1 + "-" + ball1);

                for(int i = 0; i < LayoutPredmetAdd.getChildCount(); i++)
                {
                    LinearLayout linPredmet = (LinearLayout)LayoutPredmetAdd.getChildAt(i);

                    Spinner spinn = (Spinner)linPredmet.getChildAt(0);
                    String name = spinn.getSelectedItem().toString();

                    boolean fl = false;
                    for(String r:mResultPredmet){
                        if(r.contains(name)){
                            fl = true;
                            break;
                        }
                    }

                    if(fl) break;

                    EditText etext = (EditText)linPredmet.getChildAt(1);

                    String tmBall = etext.getText().toString();
                    if(tmBall.equals("")) tmBall = "0";

                    int temp = sumBall + Integer.valueOf(tmBall);

                    String ball = String.valueOf(temp);

                    mResultPredmet.add(name + "-" + ball);
                }

                Intent mViewPodbor = new Intent(this, ViewPodbor.class);
                mViewPodbor.putExtra("result",mResultPredmet);
                startActivity(mViewPodbor);
                break;
            case R.id.item_podbor_predmet_buttonDel:
                LinearLayout lll = (LinearLayout)v.getParent();
                LayoutPredmetAdd.removeView(lll);
                mCountPredmetPoViboru--;
            break;

        }
    }
}

