package ru.kuzstu.abiturientkuzstu;

import java.io.Serializable;

public class VersionClass implements Serializable{
    private int id;
    private int version;

    public VersionClass(String _id, String _version){
        id = Integer.valueOf(_id);
        version = Integer.valueOf(_version);
    }

    public VersionClass(int _id, int _version){
        id = _id;
        version = _version;
    }

    public int getId(){
        return id;
    }

    public String getIdString(){
        return String.valueOf(id);
    }

    public int getVersion(){
        return version;
    }
}
