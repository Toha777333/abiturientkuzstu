package ru.kuzstu.abiturientkuzstu;

public class cResultPpredmet {
    public String mPredmetName;
    public String mPredmetBall;

    public cResultPpredmet(String _mPredmetName, String _mPredmetBall){
        mPredmetName = _mPredmetName;
        mPredmetBall = _mPredmetBall;
    }

    public cResultPpredmet(String[] _str){
        if(_str.length>1){
            mPredmetName = _str[0];
            mPredmetBall = _str[1];
        }else{
            mPredmetName = _str[0];
            mPredmetBall = "0";
        }
    }
}
