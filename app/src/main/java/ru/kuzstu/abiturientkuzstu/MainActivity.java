package ru.kuzstu.abiturientkuzstu;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.hardware.Camera;
import android.support.v7.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
//import android.support.v4.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.sql.Wrapper;
import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity {
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private dbHandler dbH;

    Intent intent,napravlenie,znakomstvo_kuzstu,calendar,contact,spiski,podbor,postuplenie, actMain;

    Button btnSpetialty, btnKuzstu, btnPostuplenie, btnPodbor, btnMaps, btnContact;
    public static final String LOG_TAG = "MyLog";

    public MainActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dbH = new dbHandler(this);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //картинка(шапка)
        ImageView mImageView;
        mImageView = (ImageView) findViewById(R.id.imageView1);
        mImageView.setImageResource(R.drawable.kuzstu_logo_w640);

        //Активити
        actMain = new Intent(this, MainActivity.class);
        intent = new Intent(this, MapsActivity.class);
        napravlenie = new Intent(this, SpecialtyActivity.class);
        znakomstvo_kuzstu = new Intent(this, ActivityKuzstu.class);
        calendar = new Intent(this, ActivityCalendar.class);
        contact = new Intent(this, ActivityContact.class);
        spiski = new Intent(this, ActivityLists.class);
        podbor = new Intent(this, ActivityPodbor.class);
        postuplenie = new Intent(this, ActivityPostuplenie.class);

        //Кнопки
        btnSpetialty = (Button)findViewById(R.id.buttonSpetialty);
        btnKuzstu = (Button)findViewById(R.id.buttonKuzstu);
        btnPostuplenie = (Button)findViewById(R.id.buttonPostuplenie);
        btnPodbor = (Button)findViewById(R.id.buttonPodbor);
        btnMaps = (Button)findViewById(R.id.buttonMaps);
        btnContact = (Button)findViewById(R.id.buttonContact);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();

        //Обработчик клика
        OnClickListener mOnClick = new OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.buttonContact:
                        startActivity(contact);
                        break;
                    case R.id.buttonKuzstu:
                        startActivity(znakomstvo_kuzstu);
                        break;
                    case R.id.buttonPodbor:
                        startActivity(podbor);
                        break;
                    case R.id.buttonPostuplenie:
                        startActivity(postuplenie);
                        break;
                    case R.id.buttonSpetialty:
                        startActivity(napravlenie);
                        break;
                    case R.id.buttonMaps:
                        startActivity(intent);
                        break;
                }

            }
        };

        //Присваивание кнопкам обработчика
        btnSpetialty.setOnClickListener(mOnClick);
        btnKuzstu.setOnClickListener(mOnClick);
        btnPostuplenie.setOnClickListener(mOnClick);
        btnPodbor.setOnClickListener(mOnClick);
        btnMaps.setOnClickListener(mOnClick);
        btnContact.setOnClickListener(mOnClick);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(napravlenie);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(znakomstvo_kuzstu);
                    break;
                case "Поступление":
                    startActivity(postuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(podbor);
                    break;
                case "Списки поступающих":
                    startActivity(spiski);
                    break;
                case "Карта":
                    startActivity(intent);
                    break;
                case "Календарь":
                    //startActivity(calendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(contact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        /*menu.add("Карта");
        menu.add("menu2");
        menu.add("menu3");
        menu.add("menu4");
*/
       // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;


        //return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();
        //onBackPressed();

        if(id ==R.id.action_bar_menu_drawer){

        }
       /* String id = item.toString();
        text11.setText(id);

        //noinspection SimplifiableIfStatement
        if (id == "Карта") {
           // Intent actMaps = new Intent(this, MapsActivity.class);
            startActivity(actMaps);
            return true;
        }*/



        return super.onOptionsItemSelected(item);
    }

}
