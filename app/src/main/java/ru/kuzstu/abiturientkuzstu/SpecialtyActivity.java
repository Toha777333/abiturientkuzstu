package ru.kuzstu.abiturientkuzstu;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

public class SpecialtyActivity extends ActionBarActivity {
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar, actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    AdapterSpecialty mMyAdapter;
    ExpandableListView elvMain;
    SimpleExpandableListAdapter mListAdapter;

    Intent mViewActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialty);

        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        mViewActivity = new Intent(this, ViewActivity.class);

        mMyAdapter = new AdapterSpecialty(this);
        mListAdapter = mMyAdapter.getAdapter();

        elvMain = (ExpandableListView)findViewById(R.id.elvMain);
        elvMain.setAdapter(mListAdapter);

        // нажатие на элемент
        elvMain.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition,   int childPosition, long id) {
                Log.d(LOG_TAG, "onChildClick groupPosition = " + groupPosition +
                        " childPosition = " + childPosition +
                        " id = " + id);
                String mKod = mMyAdapter.getGroupChildText(groupPosition, childPosition);
                Log.d(LOG_TAG, mKod);

                mViewActivity.putExtra("kod",mKod);
                startActivity(mViewActivity);

                return false;
            }
        });

        // нажатие на группу
        elvMain.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                Log.d(LOG_TAG, "onGroupClick groupPosition = " + groupPosition +
                        " id = " + id);
                // блокируем дальнейшую обработку события для группы с позицией 1
                //  if (groupPosition == 1) return true;

                return false;
            }
        });

        // сворачивание группы
        elvMain.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            public void onGroupCollapse(int groupPosition) {
                Log.d(LOG_TAG, "onGroupCollapse groupPosition = " + groupPosition);
                Log.d(LOG_TAG, "Свернули " + mMyAdapter.getGroupText(groupPosition));
            }
        });

        // разворачивание группы
        elvMain.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
                Log.d(LOG_TAG, "onGroupExpand groupPosition = " + groupPosition);
                Log.d(LOG_TAG,"Равзвернули " + mMyAdapter.getGroupText(groupPosition));
            }
        });

        // разворачиваем группу с позицией 2
        elvMain.expandGroup(2);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_specialty);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
