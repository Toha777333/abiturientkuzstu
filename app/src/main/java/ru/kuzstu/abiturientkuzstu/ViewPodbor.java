package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import java.util.ArrayList;
import android.support.v7.app.ActionBarDrawerToggle;

public class ViewPodbor extends ActionBarActivity implements OnClickListener{
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    ArrayList<cResultPpredmet> mPredmet;
    dbHandler dbH;
    Context ct;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_podbor);

        genericDrawerMenu();

        dbH = new dbHandler(this,true);
        ct = this;

        mPredmet = new ArrayList<cResultPpredmet>();
        ArrayList<String> temp = getIntent().getExtras().getStringArrayList("result");

        for (String t:temp){
            String[] tm = t.split("-");
            cResultPpredmet r = new cResultPpredmet(tm);
            mPredmet.add(r);
        }


        LinearLayout linLayout = (LinearLayout) findViewById(R.id.linActivitViewPodbor);
        LayoutInflater ltInflater = getLayoutInflater();

        View item;

        for(cResultPpredmet mRes: mPredmet){
            ArrayList<SpecialtyClass> mSpecialtyArr =
                    dbH.getSpecialtyInPredmetEge(mRes.mPredmetName);

            for(SpecialtyClass mSpecialty: mSpecialtyArr){
                item = ltInflater.inflate(R.layout.item_view_podbor, linLayout, false);

                TextView tvHeader = (TextView) item.findViewById(R.id.text_header_podbor_view);
                tvHeader.setText(mSpecialty.specialty_name);

                TextView tvKod = (TextView) item.findViewById(R.id.text_kod_podbor_view);
                tvKod.setText(mSpecialty.kod);

                TextView tvPredmet = (TextView) item.findViewById(R.id.text_predmet_podbor_view);
                tvPredmet.setText(mSpecialty.predmety_ege);

                TextView tvMyBall = (TextView) item.findViewById(R.id.text_my_ball_podbor_view);
                tvMyBall.setText(mRes.mPredmetBall);

                TextView tvProshliyBall =
                        (TextView) item.findViewById(R.id.text_proshliy_ball_podbor_view);
                tvProshliyBall.setText(mSpecialty.proxodnoj_ball_proshlyj_god);

                TextView tvPozaproshliyBall =
                        (TextView) item.findViewById(R.id.text_pozaproshliy_ball_podbor_view);
                tvPozaproshliyBall.setText(mSpecialty.proxodnoj_ball_pozaproshlyj_god);

                TextView tvBudget = (TextView) item.findViewById(R.id.text_budget_mest_podbor_view);
                tvBudget.setText(mSpecialty.budget_mest_ochnyx);

                TextView tvContract = (TextView) item.findViewById(R.id.text_contract_mest_podbor_view);
                tvContract.setText(mSpecialty.contract_mest_ochnyx);

                TextView tvOchnoe = (TextView) item.findViewById(R.id.text_ochnoe_many_podbor_view);
                tvOchnoe.setText(mSpecialty.stoimost_ochnogo);

                TextView tvZaochnoe = (TextView) item.findViewById(R.id.text_zaochnoe_many_podbor_view);
                tvZaochnoe.setText(mSpecialty.stoimost_zaochnogo);

                linLayout.addView(item);
                LinearLayout app_layer = (LinearLayout)item;

                app_layer.setOnClickListener(this);
            }
        }



    }

    public void genericDrawerMenu(){
        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_contact);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    public void onClick(View v) {

        LinearLayout linLayout = (LinearLayout)v;
        TextView tvKod = (TextView) linLayout.findViewById(R.id.text_kod_podbor_view);

        String mKod = tvKod.getText().toString();

        //Toast.makeText(this,tm,Toast.LENGTH_SHORT).show();
        Intent mViewActivity = new Intent(this, ViewActivity.class);
        mViewActivity.putExtra("kod",mKod);
        startActivity(mViewActivity);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_view_podbor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
