package ru.kuzstu.abiturientkuzstu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOError;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;

public class dbHandler {
    private static Context context;
    private dbConnect dbCon;
    public boolean flagRootObject,flagTaxi;
    SQLiteDatabase db;

    //ссылки на осноной сервер
    final String urlVersion_json = "http://abitur-hosting.ucoz.com/version_json.txt";
    final String urlAbitur = "http://abitur-hosting.ucoz.com/abityr.txt";

    //ссылки на запасной сервер
    final String urlVersion_json_2 = "http://abitur-hosting2.ucoz.com/_ld/0/1_version_json.txt";
    final String urlAbitur_2 = "http://abitur-hosting2.ucoz.com/_ld/0/2_abityr.txt";

    //Имя файла с данными
    String mFileNameAbitur = "abitur.txt";

    final String LOG_TAG = "myLogs";

    public dbHandler(Context context) {
        this.context = context;

        dbCon = new dbConnect(this.context);
        db = dbCon.getWritableDatabase();

        checkVersion();
    }

    public dbHandler(Context context,boolean _f) {
        this.context = context;

        dbCon = new dbConnect(this.context);
        db = dbCon.getWritableDatabase();
    }

    public int GetVersionValue(){
        Cursor c = db.query("version_json", null, null, null, null, null, null);
        int result = -1;

        if (c.moveToFirst()) {
            int _id = c.getColumnIndex("id");
            int _version = c.getColumnIndex("version");

            ArrayList<VersionClass> Version = new ArrayList<>();

            do {
                Version.add(new VersionClass(
                                c.getString(_id),
                                c.getString(_version))
                );
            } while (c.moveToNext());
            result = Version.get(0).getVersion();
        }
        return result;
    }

    public VersionClass GetVersionObject(){
        Cursor c = db.query("version_json", null, null, null, null, null, null);
        VersionClass result = new VersionClass(-1,-1);
        ArrayList<VersionClass> Version = new ArrayList<VersionClass>();

        if (c.moveToFirst()) {
            int _id = c.getColumnIndex("id");
            int _version = c.getColumnIndex("version");

            do {
                Log.e(LOG_TAG, "dbHandler: База id=" +c.getString(_id) + " версия=" + c.getString(_version));
                VersionClass temp = new VersionClass(c.getString(_id),c.getString(_version));

                Version.add(temp);
            } while (c.moveToNext());
            result = Version.get(0);
        }
        return result;
    }

    /**
     * Проверка на актуальность данных
     */
    public void checkVersion(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //скачиваем/проверяемм версию данных
                String textVersion="";
                Log.e(LOG_TAG, "dbHandler: Начинаем скачивать файл с версией");
                try {
                    URL url = new URL(urlVersion_json);
                    InputStreamReaderDecorator inputStreamReaderDecorator =
                            new InputStreamReaderDecorator(url.openStream());
                    textVersion = inputStreamReaderDecorator.readToEnd();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "dbHandler: ошибка при скачивание файла с версией у основного сервера. ", e);

                    Log.e(LOG_TAG, "dbHandler: Начинаем скачивать файл версий с зеркала");
                    try {
                        URL url = new URL(urlVersion_json_2);
                        InputStreamReaderDecorator inputStreamReaderDecorator =
                                new InputStreamReaderDecorator(url.openStream());
                        textVersion = inputStreamReaderDecorator.readToEnd();
                    } catch (IOException e2) {
                        Log.e(LOG_TAG, "dbHandler: ошибка при скачивание файла с версией. ", e2);
                        return;
                    }
                }

                if(textVersion.equals("The document has moved")){
                    //неполучилось скачать файл с версией, в моем случае кончился баланс на телефоне
                    return;
                }

                VersionClass mVersionThis = GetVersionObject();
                int mVersionServer = Integer.valueOf(textVersion);

                if(mVersionThis.getVersion()<mVersionServer){
                    Log.d(LOG_TAG, "dbHandler: Обновляем версию");
                    // создаем объект для данных
                    ContentValues cv = new ContentValues();
                    // подготовим значения для обновления
                    cv.put("version",textVersion);
                    // обновляем по id
                    int updCount = db.update("version_json", cv, "id = ?",
                            new String[] { mVersionThis.getIdString() });
                    Log.d(LOG_TAG, "dbHandler: Обновленно записей = " + updCount);

                    //загружаем новые данные
                    downloadAbitur();
                }else{
                    /*File file = new File(context.getFilesDir(), mFileNameAbitur);

                    if(!file.exists()){
                        Log.d(LOG_TAG, "dbHandler: Файл с данными не найден, начинаем скачивание");
                        downloadAbitur();
                    }*/
                }
            }
        }).start();
    }

    /**
     * Скачивание основных данных о специальностях с сервера
     */
    public void downloadAbitur(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                //скачиваем данные с сервера
                String textAbitur="";
                Log.e(LOG_TAG, "dbHandler: Начинаем скачивать файл с основными данными");
                try {
                    URL url = new URL(urlAbitur);
                    InputStreamReaderDecorator inputStreamReaderDecorator =
                            new InputStreamReaderDecorator(url.openStream());
                    textAbitur = inputStreamReaderDecorator.readToEnd();
                } catch (IOException e) {
                    Log.e(LOG_TAG, "dbHandler: ошибка при скачивание файлаосновных данных, с основного сервера. ", e);

                    Log.e(LOG_TAG, "dbHandler: Начинаем скачивать файл с основными данными с зеркала");
                    try {
                        URL url = new URL(urlAbitur_2);
                        InputStreamReaderDecorator inputStreamReaderDecorator =
                                new InputStreamReaderDecorator(url.openStream());
                        textAbitur = inputStreamReaderDecorator.readToEnd();
                    } catch (IOException e2) {
                        Log.e(LOG_TAG, "dbHandler: ошибка при скачивание файла с основными данными. ", e2);
                        return;
                    }
                }

                if(textAbitur.equals("The document has moved")){
                    //неполучилось скачать файл с версией, в моем случае кончился баланс на телефоне
                    return;
                }

                //десериализуем скачанные данные в массив
                Gson gson = new Gson();
                SpecialtyClass[] arrSpecialty;
                try{
                    arrSpecialty = gson.fromJson(textAbitur, SpecialtyClass[].class);
                }catch (IOError e){
                    Log.e(LOG_TAG, "dbHandler: ошибка при десериализации. ", e);
                    return;
                }

                //очищаем базу перед записью новых данных
                int delResult = db.delete("abitur",null,null);
                Log.e(LOG_TAG, "dbHandler: удалено записей:" + delResult);

                //запись массива в базу данных
                ContentValues cv = new ContentValues();
                for(int i=0;i<arrSpecialty.length;i++){
                /*    Log.e(LOG_TAG,
                            " "+arrSpecialty[i].kod+
                                    " "+arrSpecialty[i].specialty_name+
                                    " "+arrSpecialty[i].tip_obucheniya+
                                    " "+arrSpecialty[i].tip_ochnoe+
                                    " "+arrSpecialty[i].tip_zaochnoe+
                                    " "+arrSpecialty[i].tip_ochno_zaochnoe+
                                    " "+arrSpecialty[i].budget_mest_ochnyx+
                                    " "+arrSpecialty[i].budget_mest_zaochnyx+
                                    " "+arrSpecialty[i].contract_mest_ochnyx+
                                    " "+arrSpecialty[i].contract_mest_zaochnyx+
                                    " "+arrSpecialty[i].budget_mest_ochno_zaochnyx+
                                    " "+arrSpecialty[i].contract_mest_ochno_zaochnyx+
                                    " "+arrSpecialty[i].stoimost_ochnogo+
                                    " "+arrSpecialty[i].stoimost_zaochnogo+
                                    " "+arrSpecialty[i].stoimost_ochno_zaochnogo+
                                    " "+arrSpecialty[i].predmety_ege+
                                    " "+arrSpecialty[i].proxodnoj_ball_pozaproshlyj_god+
                                    " "+arrSpecialty[i].proxodnoj_ball_proshlyj_god+
                                    " "+arrSpecialty[i].opisanie);*/

                    cv.put("kod",arrSpecialty[i].kod);
                    cv.put("specialty_name",arrSpecialty[i].specialty_name);
                    cv.put("tip_obucheniya",arrSpecialty[i].tip_obucheniya);
                    cv.put("tip_ochnoe",arrSpecialty[i].tip_ochnoe);
                    cv.put("tip_zaochnoe",arrSpecialty[i].tip_zaochnoe);
                    cv.put("tip_ochno_zaochnoe",arrSpecialty[i].tip_ochno_zaochnoe);
                    cv.put("budget_mest_ochnyx",arrSpecialty[i].budget_mest_ochnyx);
                    cv.put("budget_mest_zaochnyx",arrSpecialty[i].budget_mest_zaochnyx);
                    cv.put("contract_mest_ochnyx",arrSpecialty[i].contract_mest_ochnyx);
                    cv.put("contract_mest_zaochnyx",arrSpecialty[i].contract_mest_zaochnyx);
                    cv.put("budget_mest_ochno_zaochnyx",arrSpecialty[i].budget_mest_ochno_zaochnyx);
                    cv.put("contract_mest_ochno_zaochnyx",arrSpecialty[i].contract_mest_ochno_zaochnyx);
                    cv.put("stoimost_ochnogo",arrSpecialty[i].stoimost_ochnogo);
                    cv.put("stoimost_zaochnogo",arrSpecialty[i].stoimost_zaochnogo);
                    cv.put("stoimost_ochno_zaochnogo",arrSpecialty[i].stoimost_ochno_zaochnogo);
                    cv.put("predmety_ege",arrSpecialty[i].predmety_ege);
                    cv.put("proxodnoj_ball_pozaproshlyj_god",arrSpecialty[i].proxodnoj_ball_pozaproshlyj_god);
                    cv.put("proxodnoj_ball_proshlyj_god",arrSpecialty[i].proxodnoj_ball_proshlyj_god);
                    cv.put("opisanie",arrSpecialty[i].opisanie);
                    db.insert("abitur", null, cv);
                }
            }
        }).start();
    }

    /**
     * Возвращает массив со всеми специальностями
     * @return
     */
    public ArrayList<SpecialtyClass> getFillDataSpecialty()
    {
        Cursor c = db.query("abitur", null, null, null, null, null, null);
        ArrayList<SpecialtyClass> result = getConvertCursotToArray(c);
        return result;
    }

    /**
     * Возвращает массив со списком специальностей бакалавров
     * @return
     */
    public ArrayList<SpecialtyClass> getBakalavr(){
        String selection = "tip_obucheniya = 'Бакалавр'";
        Cursor c = db.query("abitur", null, selection, null , null, null, null);
        ArrayList<SpecialtyClass> result = getConvertCursotToArray(c);
        return result;
    }

    /**
     * Возвращает массив со списком специальностей специалистов
     * @return
     */
    public ArrayList<SpecialtyClass> getSpecialist(){
        String selection = "tip_obucheniya = 'Специалист'";
        Cursor c = db.query("abitur", null, selection, null , null, null, null);
        ArrayList<SpecialtyClass> result = getConvertCursotToArray(c);
        return result;
    }

    /**
     * Возвращает массив со списком специальностей магистров
     * @return
     */
    public ArrayList<SpecialtyClass> getMagistr(){
        String selection = "tip_obucheniya = 'Магистр'";
        Cursor c = db.query("abitur", null, selection, null , null, null, null);
        ArrayList<SpecialtyClass> result = getConvertCursotToArray(c);
        return result;
    }

    /**
     * Возвращает массив специальностей по вытранному предмету ЕГЭ
     * @return
     */
    public ArrayList<SpecialtyClass> getSpecialtyInPredmetEge(String _predmet){
        String selection = "predmety_ege = ?";
        String[] selectionArgs = new String[] { _predmet };
        Cursor c = db.query("abitur", null, selection, selectionArgs , null, null, null);

        ArrayList<SpecialtyClass> result = getConvertCursotToArray(c);
        return result;
    }

    /**
     * Возвращает список предметов ЕГЭ
     * @return
     */
    public ArrayList<String> getPredmetEge(){
        String[] columns = new String[] { "predmety_ege" };
        String selection = "tip_obucheniya != 'Магистр'";
        String groupBy = "predmety_ege";
        Cursor c = db.query("abitur", columns, selection, null , groupBy, null, null);

        ArrayList<String> result = new ArrayList<String>();
        if (c.moveToFirst()) {
            int _predmety_ege = c.getColumnIndex("predmety_ege");

            do {
                result.add(c.getString(_predmety_ege));
            } while (c.moveToNext());
        }
        return result;
    }

    /**
     * Возвращает список предметов ЕГЭ
     * @return
     */
    public String[] getMassPredmetEge(){
        String[] columns = new String[] { "predmety_ege" };
        String selection = "tip_obucheniya != 'Магистр'";
        String groupBy = "predmety_ege";
        Cursor c = db.query("abitur", columns, selection, null , groupBy, null, null);

        String[] result = new String[c.getCount()];
        int i =0;
        if (c.moveToFirst()) {
            int _predmety_ege = c.getColumnIndex("predmety_ege");

            do {
                result[i]=c.getString(_predmety_ege);
                i++;
            } while (c.moveToNext());
        }
        return result;
    }

    /**
     * Перевод объектов из Cursor в массив
     * @param c
     * @return
     */
    public ArrayList<SpecialtyClass> getConvertCursotToArray(Cursor c)
    {
        ArrayList<SpecialtyClass> result = new ArrayList<SpecialtyClass>();

        if (c.moveToFirst()) {
            int _kod = c.getColumnIndex("kod");
            int _specialty_name = c.getColumnIndex("specialty_name");
            int _tip_obucheniya = c.getColumnIndex("tip_obucheniya");
            int _tip_ochnoe = c.getColumnIndex("tip_ochnoe");
            int _tip_zaochnoe = c.getColumnIndex("tip_zaochnoe");
            int _tip_ochno_zaochnoe = c.getColumnIndex("tip_ochno_zaochnoe");
            int _budget_mest_ochnyx = c.getColumnIndex("budget_mest_ochnyx");
            int _budget_mest_zaochnyx = c.getColumnIndex("budget_mest_zaochnyx");
            int _contract_mest_ochnyx = c.getColumnIndex("contract_mest_ochnyx");
            int _contract_mest_zaochnyx = c.getColumnIndex("contract_mest_zaochnyx");
            int _budget_mest_ochno_zaochnyx = c.getColumnIndex("budget_mest_ochno_zaochnyx");
            int _contract_mest_ochno_zaochnyx = c.getColumnIndex("contract_mest_ochno_zaochnyx");
            int _stoimost_ochnogo = c.getColumnIndex("stoimost_ochnogo");
            int _stoimost_zaochnogo = c.getColumnIndex("stoimost_zaochnogo");
            int _stoimost_ochno_zaochnogo = c.getColumnIndex("stoimost_ochno_zaochnogo");
            int _predmety_ege = c.getColumnIndex("predmety_ege");
            int _proxodnoj_ball_pozaproshlyj_god = c.getColumnIndex("proxodnoj_ball_pozaproshlyj_god");
            int _proxodnoj_ball_proshlyj_god = c.getColumnIndex("proxodnoj_ball_proshlyj_god");
            int _opisanie = c.getColumnIndex("opisanie");

            do {
                SpecialtyClass temp = new SpecialtyClass(
                        c.getString(_kod),
                        c.getString(_specialty_name),
                        c.getString(_tip_obucheniya),
                        c.getString(_tip_ochnoe),
                        c.getString(_tip_zaochnoe),
                        c.getString(_tip_ochno_zaochnoe),
                        c.getString(_budget_mest_ochnyx),
                        c.getString(_budget_mest_zaochnyx),
                        c.getString(_contract_mest_ochnyx),
                        c.getString(_contract_mest_zaochnyx),
                        c.getString(_budget_mest_ochno_zaochnyx),
                        c.getString(_contract_mest_ochno_zaochnyx),
                        c.getString(_stoimost_ochnogo),
                        c.getString(_stoimost_zaochnogo),
                        c.getString(_stoimost_ochno_zaochnogo),
                        c.getString(_predmety_ege),
                        c.getString(_proxodnoj_ball_pozaproshlyj_god),
                        c.getString(_proxodnoj_ball_proshlyj_god),
                        c.getString(_opisanie)
                );

                result.add(temp);
            } while (c.moveToNext());
        }
        return result;
    }

    /**
     * Возвращает специальность по коду
     * @param _kod код специальности
     * @return
     */
    public SpecialtyClass getSpecialty(String _kod){
        String selection = "kod = ?";
        String[] selectionArgs = new String[] { _kod };
        Cursor c = db.query("abitur", null, selection, selectionArgs , null, null, null);

        ArrayList<SpecialtyClass> temp = getConvertCursotToArray(c);
        SpecialtyClass result;

        if(temp.size()>0){
            result = temp.get(0);
        }else{
            result = new SpecialtyClass();
        }

        return result;
    }
}
