package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class ActivityKuzstu extends ActionBarActivity implements OnClickListener{
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie,
            antListInfo, actMain;

    public static final String LOG_TAG = "MyLog";

    private String[] mListKuzstu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuzstu);

        LinearLayout btKuzstu = (LinearLayout)findViewById(R.id.btKuzstu_kuzstu);
        LinearLayout btObshhezhitie = (LinearLayout)findViewById(R.id.btKuzstu_obshhezhitie);
        LinearLayout btStudlife = (LinearLayout)findViewById(R.id.btKuzstu_studlife);
        LinearLayout btProfilact = (LinearLayout)findViewById(R.id.btKuzstu_profilact);

        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Активити2
        antListInfo = new Intent(this, ActivityLists.class);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_kuzstu);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();

     /*   // находим список
        ListView lvMain = (ListView) findViewById(R.id.lvMain);

        mListKuzstu = getResources().getStringArray(R.array.title_ListView_Kuzstu);
        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mListKuzstu);

        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);
*/
        //присваиваем обработчики
        btKuzstu.setOnClickListener(this);
        btObshhezhitie.setOnClickListener(this);
        btStudlife.setOnClickListener(this);
        btProfilact.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btKuzstu_kuzstu:
                antListInfo.putExtra("view_tag","кузгту");
                startActivity(antListInfo);
                break;
            case R.id.btKuzstu_obshhezhitie:
                antListInfo.putExtra("view_tag","общежития");
                startActivity(antListInfo);
                break;
            case R.id.btKuzstu_profilact:
                antListInfo.putExtra("view_tag","профилакторий");
                startActivity(antListInfo);
                break;
            case R.id.btKuzstu_studlife:
                antListInfo.putExtra("view_tag","студенческа жизнь");
                startActivity(antListInfo);
                break;
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.menu_activity_kuzstu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
