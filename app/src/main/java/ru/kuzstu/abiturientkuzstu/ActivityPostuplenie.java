package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


public class ActivityPostuplenie extends ActionBarActivity implements OnClickListener{
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie,
            antListInfo, actMain;

    public static final String LOG_TAG = "MyLog";

    private String[] mListpostuplenie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postuplenie);

        LinearLayout btMin_ball = (LinearLayout)findViewById(R.id.btPostopl_min_ball);
        LinearLayout btPriem_dokument = (LinearLayout)findViewById(R.id.btPostopl_priem_dokument);
        LinearLayout btPodacha_doc = (LinearLayout)findViewById(R.id.btPostopl_podacha_doc);
        LinearLayout btInostrann = (LinearLayout)findViewById(R.id.btPostopl_inostrann);
        LinearLayout btZaselenie = (LinearLayout)findViewById(R.id.btPostopl_zaselenie);
        LinearLayout btPodgotovka = (LinearLayout)findViewById(R.id.btPostopl_podgotovka);

        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Активити2
        antListInfo = new Intent(this, ActivityLists.class);

     /*   // находим список
        ListView lvMain = (ListView) findViewById(R.id.lvMain);
        mListpostuplenie = getResources().getStringArray(R.array.title_ListView_postuplenie);
        // создаем адаптер
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, mListpostuplenie);
        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);*/

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_postuplenie);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();

        //события кнопок
        btMin_ball.setOnClickListener(this);
        btPriem_dokument.setOnClickListener(this);
        btPodacha_doc.setOnClickListener(this);
        btInostrann.setOnClickListener(this);
        btZaselenie.setOnClickListener(this);
        btPodgotovka.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btPostopl_min_ball:
                antListInfo.putExtra("view_tag", "минимальный балл");
                startActivity(antListInfo);
                break;
            case R.id.btPostopl_priem_dokument:
                antListInfo.putExtra("view_tag","прием документов");
                startActivity(antListInfo);
                break;
            case R.id.btPostopl_podacha_doc:
                antListInfo.putExtra("view_tag","подача документов");
                startActivity(antListInfo);
                break;
            case R.id.btPostopl_inostrann:
                antListInfo.putExtra("view_tag"," иностранцы");
                startActivity(antListInfo);
                break;
            case R.id.btPostopl_podgotovka:
                antListInfo.putExtra("view_tag","подготовка");
                startActivity(antListInfo);
                break;
            case R.id.btPostopl_zaselenie:
                antListInfo.putExtra("view_tag"," заселение");
                startActivity(antListInfo);
                break;
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_activity_postuplenie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
