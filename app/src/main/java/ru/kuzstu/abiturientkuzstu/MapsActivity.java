package ru.kuzstu.abiturientkuzstu;

import android.app.ActionBar;
import android.app.Activity;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ru.kuzstu.abiturientkuzstu.R;

public class MapsActivity extends ActionBarActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    String[] data = new String[] { "one", "two", "three" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();

            if (mMap == null) {
                finish();
                return;
            }
            setUpMap();
        }
    }

    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
     //   mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        mMap.getUiSettings().setZoomControlsEnabled(true);

        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(55.347311, 86.079157))
                .title("0 корпус (Приемная комиссия, ИДПО)) - Д.Бедного,4")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.348151, 86.077338)).title("1 корпус (Ректорат, ГИ) - Весенняя, 28")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.352523, 86.073896)).title("2 корпус (ИЭиУ) - Дзержинского, 9")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.349022, 86.080911)).title("3 корпус (ИИТМА, ИЭ) - Красноармейская, 117")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.347268, 86.080689)).title("4 корпус (СИ) - 50 лет Октября, 19")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.348300, 86.081386)).title("5 корпус (ИХНТ) - Красноармейская, 117")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.352340, 86.071699)).title("6 корпус (2 отдел, военкомат) - Дзержинского, 9б")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.347609, 86.080143)).title("7 корпус - Д.Бедного, 4а,")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.384945, 86.100649)).title("9 корпус - пр. Шахтеров 14б")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));

        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.3493886, 86.0798215)).title("Студенческая поликлиника")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.347494, 86.0793643)).title("Приемная комиcсия")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.348200, 86.079751)).title("Главная столовая КузГТУ")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.337796, 86.078934)).title("Общежитие №3 - Мичурина, 55")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.336970, 86.078354)).title("Общежитие №4 - Мичурина, 57")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));
        mMap.addMarker(new MarkerOptions().position(
                new LatLng(55.336886, 86.077108)).title("Общежитие №5 - Мичурина, 57а")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.map_marker1)));

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(55.348165, 86.077018))
                .zoom(16)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
        //mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));

    }

    public void setMoveCamera(double _x, double _y){
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(_x, _y))
                .zoom(16)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.add("Приемная комисcия");
        menu.add("0 корпус");
        menu.add("1 корпус");
        menu.add("2 корпус");
        menu.add("3 корпус");
        menu.add("4 корпус");
        menu.add("5 корпус");
        menu.add("6 корпус");
        menu.add("Общежития");

        getMenuInflater().inflate(R.menu.menu_maps, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String text = item.toString();

        switch(text){
            case "Приемная комисcия":
                setMoveCamera(55.347494, 86.0793643);
                break;
            case "0 корпус":
                setMoveCamera(55.347311, 86.079157);
                break;
            case "1 корпус":
                setMoveCamera(55.348151, 86.077338);
                break;
            case "2 корпус":
                setMoveCamera(55.352523, 86.073896);
                break;
            case "3 корпус":
                setMoveCamera(55.349022, 86.080911);
                break;
            case "4 корпус":
                setMoveCamera(55.347268, 86.080689);
                break;
            case "5 корпус":
                setMoveCamera(55.348300, 86.081386);
                break;
            case "6 корпус":
                setMoveCamera(55.352340, 86.071699);
                break;
            case "Общежития":
                setMoveCamera(55.337338, 86.078654);
                break;
            default:

        }

        return super.onOptionsItemSelected(item);
    }

}
