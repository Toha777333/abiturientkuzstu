package ru.kuzstu.abiturientkuzstu;

import android.content.ComponentName;
import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class ActivityLists extends ActionBarActivity {
    private String[] mDrawerTitle;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    Intent actMaps, actSpetialty, actKuzstu, actCalendar,
            actContact,actSpiski, actPodbor, actPostuplenie, actMain;

    public static final String LOG_TAG = "MyLog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lists);

        genericDrawerMenu();

        TextView tx = (TextView)findViewById(R.id.textInfoList);
        String temp = getIntent().getExtras().getString("view_tag");
        String ss = "";

        switch (temp) {
            case "кузгту":
                ss = getResources().getString(R.string.info_kuzstu);
                break;
            case "общежития":
                ss = getResources().getString(R.string.info_obshhezhitie);
                break;
            case "студенческа жизнь":
                ss = getResources().getString(R.string.info_studlife);
                break;
            case "профилакторий":
                ss = getResources().getString(R.string.info_profilact);
                break;
            case "минимальный балл":
                ss = getResources().getString(R.string.info_min_ball);
                break;
            case "прием документов":
                ss = getResources().getString(R.string.info_data_priema_doc);
                break;
            case "подача документов":
                ss = getResources().getString(R.string.info_podacha_doc);
                break;
            case " иностранцы":
                ss = getResources().getString(R.string.info_inostrann);
                break;
            case "подготовка":
                ss = getResources().getString(R.string.info_podgotovka);
                tx.setText(Html.fromHtml(ss));
                Linkify.addLinks(tx, Linkify.PHONE_NUMBERS);
                return;
            case " заселение":
                ss = getResources().getString(R.string.info_zaselenie);
                break;
        }

        tx.setText(Html.fromHtml(ss));

    }

    public void genericDrawerMenu(){
        //Активити
        actMain = new Intent(this, MainActivity.class);
        actMaps = new Intent(this, MapsActivity.class);
        actSpetialty = new Intent(this, SpecialtyActivity.class);
        actKuzstu = new Intent(this, ActivityKuzstu.class);
        actCalendar = new Intent(this, ActivityCalendar.class);
        actContact = new Intent(this, ActivityContact.class);
        actSpiski = new Intent(this, ActivityLists.class);
        actPodbor = new Intent(this, ActivityPodbor.class);
        actPostuplenie = new Intent(this, ActivityPostuplenie.class);

        //Боковое меню
        mDrawerTitle = getResources().getStringArray(R.array.title_drawer_item_menu);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_contact);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mDrawerTitle));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //Настройка actionBar для установления в нем кнопки вызова меню
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setDisplayShowHomeEnabled(true);
        //Добавление кнопки в actionBar для вызова бокового меню
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,        /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        );
        //ХЗ что это
        mDrawerToggle.syncState();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            TextView TitleSelectItem = (TextView)view;

            switch(TitleSelectItem.getText().toString()){
                case "Главная":
                    startActivity(actMain);
                    finish();
                    break;
                case "Направления подготовки":
                    startActivity(actSpetialty);
                    break;
                case "Знакомство с КузГТУ":
                    startActivity(actKuzstu);
                    break;
                case "Поступление":
                    startActivity(actPostuplenie);
                    break;
                case "Подбор специальностей по ЕГЭ":
                    startActivity(actPodbor);
                    break;
                case "Списки поступающих":
                    startActivity(actSpiski);
                    break;
                case "Карта":
                    startActivity(actMaps);
                    break;
                case "Календарь":
                    //startActivity(actCalendar);
                    ComponentName cn;
                    Intent i = new Intent();
                    cn = new ComponentName("com.android.calendar", "com.android.calendar.LaunchActivity");
                    i.setComponent(cn);
                    startActivity(i);
                    break;
                case "Контакты":
                    startActivity(actContact);
                    break;
                default:
                    Log.e(LOG_TAG, "пункт не описан или отсутствует");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_activity_lists, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
