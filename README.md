# ЗАДАЧИ#

* заполнить список специальностей
* сделать отображение номеров/контактов
* выводить подробную информацию о специальностях
* сделать главную страницу
* сообразить базу специальностей
* сделать подбор по ЕГЭ


**добавить список банков принимающих платежи для политеха и указать проценты

### СПИСОК СПЕЦИАЛЬНОСТЕЙ ###
**Перечень направлений подготовки очного обучения, на которые КузГТУ объявляет прием в 2015 году.
Срок обучения 4 года.**


* Строительство
* Информационные системы и технологии
* Прикладная информатика
* Теплоэнергетика и теплотехника
* Электроэнергетика и электротехника
* Машиностроение
* Автоматизация технологических процессов и производств
* Конструкторско-технологическое обеспечение машиностроительных производств
* Химическая технология
* Энерго- и ресурсосберегающие процессы в химической
* технологии, нефтехимии и биотехнологии
* Техносферная безопасность
* Землеустройство и кадастры
* Технология транспортных процессов
* Эксплуатация транспортно-технологических машин и комплексов
* Управление качеством
* Экономика
* Менеджмент
* Государственное и муниципальное управление
* Сервис

**Перечень направлений подготовки заочного обучения, на которые КузГТУ объявляет прием в 2015 году. Срок обучения 5 лет**

* Строительство
* Теплоэнергетика и теплотехника
* Электроэнергетика и электротехника
* Машиностроение
* Конструкторско-технологическое обеспечение машиностроительных производств
* Химическая технология
* Энерго- и ресурсосберегающие процессы в химической
* технологии, нефтехимии и биотехнологии
* Техносферная безопасность
* Технология транспортных процессов
* Эксплуатация транспортно-технологических машин и комплексов
* Экономика
* Менеджмент
* Государственное и муниципальное управление
* Сервис


**Перечень специальностей очного обучения, на которые КузГТУ объявляет прием в 2015 году.**

* Строительство уникальных зданий и сооружений
* Прикладная геология
* Горное дело Физические процессы горного или нефтегазового производства
* Экономическая безопасность

**Перечень специальностей заочного обучения, на которые КузГТУ объявляет прием в 2015 году.**

* Горное дело
* Экономическая безопасность


**Перечень направлений подготовки магистров, на которые КузГТУ объявляет прием в 2015 году.**

* Строительство
* Информационные системы и технологии
* Прикладная информатика
* Теплоэнергетика и теплотехника
* Электроэнергетика и электротехника
* Машиностроение
* Автоматизация технологических процессов и производств
* Конструкторско-технологическое обеспечение машиностроительных производств
* Химическая технология
* Энерго- и ресурсосберегающие процессы в химической технологии, нефтехимии и биотехнологии
* Техносферная безопасность
* Землеустройство и кадастры
* Технология транспортных процессов
* Управление качеством
* Экономика
* Менеджмент
* Государственное и муниципальное управление
* Сервис

<item>Русский язык</item>
<item>Математика</item>
<item>Физика</item>
<item>Химия</item>
<item>Информатика и ИКТ</item>
<item>Биология</item>
<item>История</item>
<item>География</item>
<item>Английский язык</item>
<item>Немецкий язык</item>
<item>Французский язык</item>
<item>Обществознание</item>
<item>Испанский язык</item>
<item>Литература</item>


**ПРИМЕРЫ КОДА**

/* LinearLayout.LayoutParams par  = new LinearLayout.LayoutParams(
        LinearLayout.LayoutParams.MATCH_PARENT,
        LinearLayout.LayoutParams.WRAP_CONTENT);
//mImageView.setLayoutParams(par);*/

/* if(actionbar != null) {
    Toast.makeText(this,"NULL",Toast.LENGTH_LONG).show();//showDialog(Toast.LENGTH_LONG);

}*/

код									- kod
специальность						- specialty_name
тип обучения						- tip_obucheniya
очное								- tip_ochnoe
заочное								- tip_zaochnoe
бюджетных мест очных				- budget_mest_ochnyx
бюджетных месть заочных				- budget_mest_zaochnyx
контрактных мест очных				- contract_mest_ochnyx
контрактных мест заочных			- contract_mest_zaochnyx
бюджетных мест очно-заочных			- budget_mest_ochno_zaochnyx
контрактных мест очно-заочных		- contract_mest_ochno_zaochnyx
стоимость очного обучения			- stoimost_ochnogo
стоимость заочного обучения			- stoimost_zaochnogo
стоимость очно-заочного обучения	- stoimost_ochno_zaochnogo
предметы по ЕГЭ						- predmety_ege
проходной балл позапрошлый год		- proxodnoj_ball_pozaproshlyj_god
проходной балл прошлый год			- proxodnoj_ball_proshlyj_god
описание							- opisanie